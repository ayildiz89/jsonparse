node('master')  {
    stage ('git checkout') {
        checkout scmGit(branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'test-creds', url: 'git@gitlab.com:ayildiz89/jsonparse.git']])

    }
       stage ('run script') {
        sh "pwd" 
        sh "env"
        //def workspace = env.WORKSPACE
        dir ('workspace') {
               sh '${WORKSPACE}/./test.sh'
       }
       }
        stage('Send JSON file to Logstash') {
           
                    logstashSend {
                        inputFile('path/to/json/file.json') // Specify the path to the JSON file
                        contentType('application/json') // Specify the content type (optional, default is 'text/plain')
                        logstashUrl('http://logstash-server:port') // Specify the Logstash server URL (optional if configured globally)
                        logstashIndex('jenkins-logs') // Specify the index name in Logstash (optional)
                        additionalFields([key1: 'value1', key2: 'value2']) // Add any additional fields to the log events (optional)
                        failOnError(true) // Specify whether to fail the build on Logstash errors (optional)
                    }
                }
         

      // stage ('Creating JSON') {
    
                  //  def sldata = readJSON text: '[{ "component_name": "frontend", "buildSessionId": "123" },{ "component_name": "backend", "buildSessionId": "456" }]'
                    
                  //  writeJSON(file: 'sl-ib-components.json', json: sldata )
            //     }
             
        
       
   //   stage('Validate JSON') {
    
   //             sh "cat sl-ib-components.json | jq ."
    //     }
      
}