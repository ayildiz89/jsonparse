#!/bin/bash

DOMAIN="mycompany.sealights.co"
SL_API_TOKEN=`cat sl_api_token.txt`
SL_AGENT_TOKEN=`cat sl_agent_token.txt`

INTEGRATION_BUILD_NAME="myIntegrationBuildSample"
QA_ENV="QA-01"

#Retrieve the Sealights Generated LabID based on App and Environment name
SL_LABID=`curl -sX GET "https://$DOMAIN/sl-api/v1/lab-ids?appName=$INTEGRATION_BUILD_NAME&branchName=$QA_ENV&buildType=integration" \
                -H "Authorization: Bearer $SL_API_TOKEN" -H "accept: application/json" | jq --raw-output .data.labIds[0].labId`
echo "Sealights' LabID is $SL_LABID"

#Prepare list of microservices in environment
curl -sX GET "https://$DOMAIN/sl-api/v1/agents/live" -H "Authorization: Bearer $SL_API_TOKEN" -H "accept: application/json">liveagents.txt
cat liveagents.txt | jq -c --arg LABID_PARAM $SL_LABID '.data[] | select(.labId==$LABID_PARAM and .type=="TestListener")' | jq -s 'group_by(.bsid)[] | {appName: .[0].appName, branch: .[0].branchName, build: .[0].buildName}' | jq -s>sl-integration-components.json

echo "List of components generated for the $INTEGRATION_BUILD_NAME on $QA_ENV"
cat sl-integration-components.json

#Download the java agent if not installed already
if [ ! -f "sealights-java-latest.zip" ]; then
    echo "Downloading Sealights Agent..."
    wget -nv  https://agents.sealights.co/sealights-java/sealights-java-latest.zip
    unzip -oq sealights-java-latest.zip
    echo "Local agent version is now:" `cat sealights-java-version.txt`
fi
                   
#Report new Integration Build to SL ("Picture" of ms deployed in the environment)
echo "Reporting new version of $INTEGRATION_BUILD_NAME (for $QA_ENV) to Sealights"
java -jar sl-build-scanner.jar -config -token $SL_AGENT_TOKEN -appname $INTEGRATION_BUILD_NAME -branchname $QA_ENV -buildname `date +"%y%m%d_%H%M"` -pi "*integration.build*" -buildsessionidfile integrationBSID.txt
java -jar sl-build-scanner.jar -scan -token $SL_AGENT_TOKEN -buildsessionidfile integrationBSID.txt -componentfile sl-integration-components.json 

echo Script is complete: $QA_ENV can be tested.