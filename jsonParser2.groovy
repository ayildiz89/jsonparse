import groovy.json.*
import jenkins.plugins.http_request.HttpRequest

node('master')  {
    stage ('git checkout') {
        checkout scmGit(branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'test-creds', url: 'git@gitlab.com:ayildiz89/jsonparse.git']])
    }
    def newJson = '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json'

   // echo newJson

    httpRequest (
        contentType: 'APPLICATION_JSON',
        httpMode: 'POST',
        //requestBody: newJson,
        responseHandle: 'NONE',
         multipartName: '/var/jenkins_home/workspace/dynamic_id_copy_file_to_another/performance-results.json',
        url: 'http://192.168.178.150:9200/integration_test_3/_doc',
        validResponseCodes: "200,404,201"
    )
}
