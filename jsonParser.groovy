import groovy.json.*
import jenkins.plugins.http_request.HttpRequest

node('master')  {
    stage ('git checkout') {
        checkout scmGit(branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'test-creds', url: 'git@gitlab.com:ayildiz89/jsonparse.git']])
    }
    def suite
    def newJson
    def code
  

    //parse json data 
    stage('ParseJson') {

        def jsonFilePath = '/var/jenkins_home/workspace/logstashtest/package.json'
        @NonCPS
          def parseJsonFile = { filePath ->
                def fileContents = readFile file: filePath
                def slurper = new JsonSlurper()
                slurper.parseText(fileContents)
            }
       def  json = parseJsonFile(jsonFilePath)
       //echo "Country: ${json.DE.ptm0101.country}"
       //echo "Branch: ${json.branch}"
       code = json.DE.toString()
       suite = code.substring(1, 8)
     
       
      // echo suite
      // echo code 
 //if (newData.Country == null) {
                //newData.Country = slurper.parseText("{}")
                //json.put(country, newData.Country)

        // Create a new Groovy map for the new JSON data
        @NonCPS
        def newData = [:]
        // create the map with desired data
        newData.Branch = json.branch
        newData.Date = json.date
        newData.TillVersion = json.tillVersion
        newData.Suite = suite
        newData.Country = json.DE.ptm0101.country
        newData.ExecutionTime = json.DE.ptm0101.executionTime
        newData.ArticleCount = json.DE.ptm0101.indicators.articleCount
        newData.ReceiptCount = json.DE.ptm0101.indicators.receiptCount
        newData.Duration = json.DE.ptm0101.processDuration
       
       // Convert the map to JSON
       newJson = JsonOutput.toJson(newData)
     
      
       // Print the new JSON data
       echo newJson
       }

       // sent output logs to logstash
       stage("logstash"){ 
      
        httpRequest (
            //consoleLogResponseBody: true,
            contentType: 'APPLICATION_JSON',
            httpMode: 'POST',
            requestBody: newJson,
            responseHandle: 'NONE',
            url: 'http://192.168.178.150:9200/integration_test/_doc',
            validResponseCodes: "200,404,201"
        )
                        
                        //sh 'curl -XPOST -H "Content-Type:  application/json" -d \'' + groovy.json.JsonOutput.toJson(newJson) + '\' http://192.168.178.150:9200/jenkins_builds_data/_doc'
                       
    
    

}
 }
